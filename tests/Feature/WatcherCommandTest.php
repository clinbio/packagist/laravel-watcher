<?php

namespace Dterumal\Watcher\Tests\Feature;

use Dterumal\Watcher\Events\FileEvent;
use Dterumal\Watcher\Events\WatcherCreated;
use Dterumal\Watcher\Events\WatcherStopped;
use Dterumal\Watcher\Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\File;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Process\Process;

class WatcherCommandTest extends TestCase
{
    /** @test */
    function it_can_start_the_watcher_with_a_timeout()
    {
        Event::fake();

        // Directory watched
        $directory = storage_path('app');

        // Run the artisan command
        $exitCode = Artisan::call('watch:run', [
            '--timeout' => 1
        ], new NullOutput);

        // Check that exit code is zero
        $this->assertSame(0, $exitCode);

        // Check that the watcher was created
        Event::assertDispatched(WatcherCreated::class, function ($event) use ($directory) {
            return $event->directory === $directory;
        });

        // Check that the watcher was stopped
        Event::assertDispatched(WatcherStopped::class, function ($event) {
            return $event->status === 0;
        });
    }

    /** @test */
    function it_detects_a_file()
    {
        Event::fake();

        // Directory watched
        $directory = storage_path('app');

        // File created
        $fooFile = storage_path('app/test.txt');

        // make sure we're starting from a clean state
        if (File::exists($fooFile)) {
            unlink($fooFile);
        }

        // Start background process
        $process = new Process(['/bin/bash', 'sleep-and-create-a-file.sh', $fooFile], $this->getStubDirectory());
        $process->start();

        // Run the artisan command
        $exitCode = Artisan::call('watch:run', [
            '--timeout' => 3
        ], new NullOutput);

        // Assert a new file is created
        $this->assertTrue(File::exists($fooFile));
        unlink($fooFile);

        // Check that exit code
        $this->assertSame(0, $exitCode);

        // Check that the watcher was created
        Event::assertDispatched(FileEvent::class, function ($event) use ($directory) {
            return $event->watcher === 'default' &&
                $event->mask === 256 &&
                $event->cookie === 0 &&
                $event->filename === 'test.txt' &&
                $event->path === $directory;
        });
    }

    /** @test */
    function it_detects_a_file_and_exits_with_option_once()
    {
        Event::fake();

        // Directory watched
        $directory = storage_path('app');

        // File created
        $fooFile = storage_path('app/test.txt');

        // make sure we're starting from a clean state
        if (File::exists($fooFile)) {
            unlink($fooFile);
        }

        // Start background process
        $process = new Process(['/bin/bash', 'sleep-and-create-a-file.sh', $fooFile], $this->getStubDirectory());
        $process->start();

        // Run the artisan command
        $exitCode = Artisan::call('watch:run', [
            '--once' => true
        ], new NullOutput);

        // Assert a new file is created
        $this->assertTrue(File::exists($fooFile));
        unlink($fooFile);

        // Check that exit code
        $this->assertSame(0, $exitCode);

        // Check that the watcher was created
        Event::assertDispatched(FileEvent::class, function ($event) use ($directory) {
            return $event->watcher === 'default' &&
                $event->mask === 256 &&
                $event->cookie === 0 &&
                $event->filename === 'test.txt' &&
                $event->path === $directory;
        });

        // Check that the watcher was stopped
        Event::assertDispatched(WatcherStopped::class, function ($event) {
            return $event->status === 0;
        });
    }
}
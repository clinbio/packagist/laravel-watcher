<?php

namespace Dterumal\Watcher\Tests;

use Dterumal\Watcher\WatcherServiceProvider;
use Illuminate\Foundation\Testing\Concerns\InteractsWithConsole;
use Illuminate\Foundation\Testing\Concerns\InteractsWithContainer;

class TestCase extends \Orchestra\Testbench\TestCase
{
    use InteractsWithContainer, InteractsWithConsole;

    public function setUp(): void
    {
        parent::setUp();
        // additional setup
    }

    protected function getPackageProviders($app)
    {
        return [
            WatcherServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        // perform environment setup
    }

    public function getStubDirectory(): string
    {
        return __DIR__.'/stubs';
    }
}
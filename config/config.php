<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Watcher Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may define the watcher settings used by your application
    | in all environments. Define as many watcher as you need to. Each
    | watcher will act independently.
    |
    */

    'watchers' => [
        'default' => [
            'path' => storage_path('app'),
            'mask' => 256 //IN_CREATE
        ],
    ],

];

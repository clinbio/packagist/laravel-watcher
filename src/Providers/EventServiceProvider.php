<?php

namespace Dterumal\Watcher\Providers;

use Dterumal\Watcher\Events\FileEvent;
use Dterumal\Watcher\Events\WatcherCreated;
use Dterumal\Watcher\Events\WatcherRestarted;
use Dterumal\Watcher\Events\WatcherStopped;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        WatcherCreated::class => [
            //
        ],
        WatcherStopped::class => [
            //
        ],
        WatcherRestarted::class => [
            //
        ],
        FileEvent::class => [
            //
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
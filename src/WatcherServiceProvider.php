<?php

namespace Dterumal\Watcher;

use Dterumal\Watcher\Console\RestartCommand;
use Dterumal\Watcher\Console\StopCommand;
use Dterumal\Watcher\Console\TestWatchCommand;
use Dterumal\Watcher\Console\WatchCommand;
use Dterumal\Watcher\Console\WatchListCommand;
use Dterumal\Watcher\Providers\EventServiceProvider;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\ServiceProvider;

class WatcherServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Watcher::class, function ($app) {
            $isDownForMaintenance = function () {
                return $this->app->isDownForMaintenance();
            };

            return new Watcher(
                $app['events'],
                $app[ExceptionHandler::class],
                $isDownForMaintenance
            );
        });

        $this->app->register(EventServiceProvider::class);

        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'watcher');
    }

    public function boot()
    {
        $this->commands([
            RestartCommand::class,
            StopCommand::class,
            WatchCommand::class,
            WatchListCommand::class,
            TestWatchCommand::class
        ]);

        if ($this->app->runningInConsole()) {

            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('watcher.php'),
            ], 'config');

        }
    }
}
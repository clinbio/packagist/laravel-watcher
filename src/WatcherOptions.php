<?php

namespace Dterumal\Watcher;

class WatcherOptions
{
    /**
     * The number of seconds to wait in between polling the queue.
     *
     * @var mixed
     */
    public $sleep;

    /**
     * The number of seconds to rest between jobs.
     *
     * @var mixed
     */
    public $rest;

    /**
     * Indicates if the watcher should run in maintenance mode.
     *
     * @var mixed
     */
    public $force;

    /**
     * Indicates if the watcher should stop after a specific elapsed time (in seconds).
     *
     * @var mixed
     */
    public $timeout;

    /**
     * Indicates if the watcher should stop after one event.
     *
     * @var mixed
     */
    public $once;

    /**
     * Create a new watcher options instance.
     *
     * @param  mixed  $sleep
     * @param  mixed  $force
     * @param  mixed  $rest
     * @return void
     */
    public function __construct($sleep = 5, $force = false, $rest = 0, $timeout = 0, $once = false)
    {
        $this->sleep = $sleep;
        $this->rest = $rest;
        $this->force = $force;
        $this->timeout = $timeout;
        $this->once = $once;
    }
}

<?php

namespace Dterumal\Watcher;


use Dterumal\Watcher\Events\FileEvent;
use Dterumal\Watcher\Events\WatcherCreated;
use Dterumal\Watcher\Events\WatcherRestarted;
use Dterumal\Watcher\Events\WatcherStopped;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Contracts\Cache\Repository as CacheContract;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Testing\Fakes\EventFake;

class Watcher
{
    public const EXIT_SUCCESS = 0;

    /**
     * The event dispatcher instance.
     *
     * @var \Illuminate\Contracts\Events\Dispatcher|EventFake
     */
    protected $events;

    /**
     * The cache repository implementation.
     *
     * @var Repository
     */
    protected Repository $cache;

    /**
     * The exception handler instance.
     *
     * @var ExceptionHandler
     */
    protected ExceptionHandler $exceptions;

    /**
     * The callback used to determine if the application is in maintenance mode.
     *
     * @var callable
     */
    protected $isDownForMaintenance;

    /**
     * The inotify instance
     *
     * @var resource|closed-resource
     */
    protected $inotify;

    /**
     * Timestamp of last restart
     *
     * @var int|null
     */
    protected ?int $lastRestart;

    /**
     * Should the watcher stop
     *
     * @var bool
     */
    protected bool $shouldQuit = false;

    /**
     * Watchers
     *
     * @var array
     */
    protected array $watchers = [];

    /**
     * Create a new directory watcher.
     *
     * @param  Dispatcher|EventFake  $events
     * @param  ExceptionHandler  $exceptions
     * @param  callable  $isDownForMaintenance
     */
    public function __construct(
        $events,
        ExceptionHandler $exceptions,
        callable $isDownForMaintenance
    ) {
        $this->events = $events;
        $this->exceptions = $exceptions;
        $this->isDownForMaintenance = $isDownForMaintenance;
    }

    /**
     * Execute the console command.
     *
     * @param  WatcherOptions  $options
     * @return int|null
     */
    public function daemon(WatcherOptions $options)
    {
        if ($this->supportsAsyncSignals()) {
            $this->listenForSignals();
        }

        $this->run();

        $this->lastRestart = $this->getTimestampOfLastWatchRestart();

        $timeStart = microtime(true);

        while (true) {
            if (!$this->daemonShouldRun($options)) {
                $status = $this->pauseWatcher($options, $this->lastRestart);

                if (!is_null($status)) {
                    return $this->stop($status);
                }

                continue;
            }

            if ((int) $options->timeout !== 0) {
                if ((microtime(true) - $timeStart) > (int) $options->timeout) {
                    $this->shouldQuit = true;
                }
            }

            $events = inotify_read($this->inotify);

            if (!empty($events)) {
                foreach ($events as $event) {
                    [$watcher, $filename, $mask, $cookie] = [
                        $this->watchers[$event['wd']], $event['name'], $event['mask'], $event['cookie']
                    ];

                    $this->raiseOnFileEvent(
                        $watcher,
                        $filename,
                        $mask,
                        $cookie
                    );

                    if ($options->once) {
                        $this->shouldQuit = true;
                    }

                    if ($options->rest > 0) {
                        $this->sleep($options->rest);
                    }
                }
            }
            $this->sleep($options->sleep);

            $status = $this->stopIfNecessary(
                $this->lastRestart
            );

            if (!is_null($status)) {
                return $this->stop($status);
            }
        }
    }

    /**
     * Determine if the daemon should process on this iteration.
     *
     * @param  WatcherOptions  $options
     * @return bool
     */
    protected function daemonShouldRun(WatcherOptions $options): bool
    {
        return !(($this->isDownForMaintenance)() && !$options->force);
    }

    /**
     * Start a inotify instance
     *
     * @return void
     */
    protected function run(): void
    {
        $this->cache->forever('watcher:stop', false);

        $this->inotify = inotify_init();

        stream_set_blocking($this->inotify, false);

        $this->startWatchers();
    }

    /**
     * Stop a inotify instance
     *
     * @return void
     */
    protected function kill(): void
    {
        $this->stopWatchers();

        fclose($this->inotify);
    }

    /**
     * Pause the worker for the current loop.
     *
     * @param  WatcherOptions  $options
     * @param  int  $lastRestart
     * @return int|null
     */
    protected function pauseWatcher(WatcherOptions $options, int $lastRestart): ?int
    {
        $this->sleep($options->sleep > 0 ? $options->sleep : 1);

        return $this->stopIfNecessary($lastRestart);
    }

    /**
     * Sleep the script for a given number of seconds.
     *
     * @param  mixed  $seconds
     * @return void
     */
    public function sleep($seconds): void
    {
        sleep($seconds);
    }

    /**
     * Determine the exit code to stop the process if necessary.
     *
     * @param  int|null  $lastRestart
     * @return int|null
     */
    protected function stopIfNecessary(?int $lastRestart): ?int
    {
        if ($this->shouldQuit || $this->watcherShouldStop()) {
            $this->kill();
            return static::EXIT_SUCCESS;
        }

        if ($this->watcherShouldRestart($lastRestart)) {
            $this->restartWatchers();
        }
        return null;
    }

    /**
     * Start the watchers
     *
     * @return void
     */
    protected function startWatchers(): void
    {
        $paths = [];

        foreach ($this->directories() as $identifier => $value) {
            $paths[] = $value['path'];
            $watcher = inotify_add_watch(
                $this->inotify,
                $value['path'],
                $value['mask']
            );

            $this->watchers[$watcher] = $identifier;
            $this->raiseAfterWatcherCreation($value['path']);
        }

        $this->registerWatchers($paths);
    }

    /**
     * Stop the watchers
     *
     * @return void
     */
    protected function stopWatchers(): void
    {
        foreach (array_keys($this->watchers) as $watcher) {
            if (inotify_rm_watch($this->inotify, $watcher)) {
                unset($this->watchers[$watcher]);
            }
        }

        $this->unregisterWatchers();
    }

    /**
     * Determine if the queue worker should restart.
     *
     * @param  int|null  $lastRestart
     * @return bool
     */
    protected function watcherShouldRestart(?int $lastRestart): bool
    {
        return $this->getTimestampOfLastWatchRestart() !== $lastRestart;
    }

    /**
     * Determine if the queue worker should stop.
     *
     * @return bool
     */
    protected function watcherShouldStop(): bool
    {
        return !($this->getStatusOfLastStop() === null) && $this->getStatusOfLastStop();
    }

    /**
     * Get the last queue restart timestamp, or null.
     *
     * @return int|null
     */
    protected function getTimestampOfLastWatchRestart(): ?int
    {
        return $this->cache->get('watcher:restart');
    }

    /**
     * Get the stop status of the watcher, or null.
     *
     * @return bool|null
     */
    protected function getStatusOfLastStop(): ?bool
    {
        return $this->cache->get('watcher:stop');
    }

    /**
     * Enable async signals for the process.
     *
     * @return void
     */
    protected function listenForSignals(): void
    {
        pcntl_async_signals(true);

        pcntl_signal(SIGTERM, function () {
            $this->shouldQuit = true;
        });
    }

    /**
     * Determine if "async" signals are supported.
     *
     * @return bool
     */
    protected function supportsAsyncSignals(): bool
    {
        return extension_loaded('pcntl');
    }

    /**
     * Restart the watchers
     *
     * @return void
     */
    protected function restartWatchers(): void
    {
        $this->kill();

        $this->run();

        $this->lastRestart = $this->getTimestampOfLastWatchRestart();

        $this->raiseAfterWatcherRestart($this->lastRestart);
    }

    /**
     * Register the watchers
     *
     * @param  array  $paths
     * @return void
     */
    protected function registerWatchers(array $paths): void
    {
        $this->cache->forever('watch:list', $paths);
    }

    /**
     * Register the watchers
     *
     * @return void
     */
    protected function unregisterWatchers(): void
    {
        $this->cache->forget('watch:list');
    }

    /**
     * Get directories to watch.
     *
     * @return array
     */
    protected function directories(): array
    {
        return config('watcher.watchers');
    }

    /**
     * Stop listening and bail out of the script.
     *
     * @param  int  $status
     * @return int
     */
    public function stop($status = 0): int
    {
        $this->events->dispatch(new WatcherStopped($status));

        return $status;
    }

    /**
     * Raise after a watcher creation.
     *
     * @param  string  $watcher
     * @param  string  $filename
     * @param  int  $mask
     * @param  int  $cookie
     * @return void
     */
    protected function raiseOnFileEvent(string $watcher, string $filename, int $mask, int $cookie): void
    {
        $this->events->dispatch(new FileEvent(
            $watcher,
            $filename,
            $mask,
            $cookie
        ));
    }

    /**
     * Raise after a watcher creation.
     *
     * @param  string  $directory
     * @return void
     */
    protected function raiseAfterWatcherCreation(string $directory): void
    {
        $this->events->dispatch(new WatcherCreated(
            $directory
        ));
    }

    /**
     * Raise after a watcher creation.
     *
     * @param  int  $lastRestart
     * @return void
     */
    protected function raiseAfterWatcherRestart(int $lastRestart): void
    {
        $this->events->dispatch(new WatcherRestarted(
            $lastRestart
        ));
    }

    /**
     * Set the cache repository implementation.
     *
     * @param  CacheContract  $cache
     * @return $this
     */
    public function setCache(CacheContract $cache): Watcher
    {
        $this->cache = $cache;

        return $this;
    }
}

<?php

namespace Dterumal\Watcher\Events;

class WatcherRestarted
{
    /**
     * The last restart
     *
     * @var int
     */
    public int $lastRestart;

    /**
     * Create a new event instance.
     *
     * @param  int  $lastRestart
     */
    public function __construct(int $lastRestart)
    {
        $this->lastRestart = $lastRestart;
    }
}

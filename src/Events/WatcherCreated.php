<?php

namespace Dterumal\Watcher\Events;

class WatcherCreated
{
    /**
     * The watched directory
     *
     * @var string
     */
    public string $directory;

    /**
     * Create a new event instance.
     *
     * @param  string  $directory
     */
    public function __construct(string $directory)
    {
        $this->directory = $directory;
    }
}

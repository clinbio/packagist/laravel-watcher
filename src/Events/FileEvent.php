<?php

namespace Dterumal\Watcher\Events;

class FileEvent
{
    /**
     * The watcher identifier
     *
     * @var string
     */
    public string $watcher;

    /**
     * The file path
     *
     * @var string
     */
    public string $path;

    /**
     * The file mask
     *
     * @var int
     */
    public int $mask;

    /**
     * The file name
     *
     * @var string
     */
    public string $filename;

    /**
     * The file cookie
     *
     * @var int
     */
    public int $cookie;

    /**
     * Create a new event instance.
     *
     * @param  string  $watcher
     * @param  string  $filename
     * @param  int  $mask
     * @param  int  $cookie
     */
    public function __construct(
        string $watcher,
        string $filename,
        int $mask,
        int $cookie
    ) {
        $this->watcher = $watcher;
        $this->filename = $filename;
        $this->mask = $mask;
        $this->cookie = $cookie;
        $this->path = collect(config('watcher.watchers'))->first(function ($value, $key) {
            return $key === $this->watcher;
        })['path'];
    }
}

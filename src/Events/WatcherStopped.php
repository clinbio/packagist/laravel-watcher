<?php

namespace Dterumal\Watcher\Events;

class WatcherStopped
{
    /**
     * The exit status.
     *
     * @var int
     */
    public int $status;

    /**
     * Create a new event instance.
     *
     * @param  int  $status
     * @return void
     */
    public function __construct(int $status)
    {
        $this->status = $status;
    }
}

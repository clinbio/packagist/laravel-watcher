<?php

namespace Dterumal\Watcher\Console;

use Illuminate\Console\Command;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Support\Arr;
use Symfony\Component\Console\Input\InputOption;

class WatchListCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'watch:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all registered watchers';

    /**
     * The cache store implementation.
     *
     * @var Repository
     */
    protected Repository $cache;

    /**
     * The table headers for the command.
     *
     * @var string[]
     */
    protected array $headers = ['Path'];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Cache $cache)
    {
        parent::__construct();

        $this->cache = $cache;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        if (is_null($this->cache->get('watch:list'))) {
            $this->error("Your application doesn't have any watchers running.");
        }

        if (empty($watchers = $this->getWatchers())) {
            $this->error("Your application doesn't have any watchers matching the given criteria.");
        }

        $this->displayWatchers($watchers);
    }

    /**
     * Compile the watchers into a displayable format.
     *
     * @return array
     */
    protected function getWatchers(): ?array
    {
        $cachedWatchers = $this->cache->get('watch:list');

        $watchers = collect($cachedWatchers)->map(function ($watcher) {
            return [
                'path'          => $watcher
            ];
        })->filter()->all();

        $watchers = $this->sortWatchers('path', $watchers);

        return $this->pluckColumns($watchers);
    }

    /**
     * Sort the routes by a given element.
     *
     * @param  string  $sort
     * @param  array  $watchers
     * @return array
     */
    protected function sortWatchers(string $sort, array $watchers): array
    {
        return Arr::sort($watchers, function ($watcher) use ($sort) {
            return $watcher[$sort];
        });
    }

    /**
     * Remove unnecessary columns from the routes.
     *
     * @param  array  $watchers
     * @return array
     */
    protected function pluckColumns(array $watchers): array
    {
        return array_map(function ($watcher) {
            return Arr::only($watcher, $this->getColumns());
        }, $watchers);
    }

    /**
     * Display the route information on the console.
     *
     * @param  array  $routes
     * @return void
     */
    protected function displayWatchers(array $routes): void
    {
        $this->table($this->getHeaders(), $routes);
    }

    /**
     * Get the table headers for the visible columns.
     *
     * @return array
     */
    protected function getHeaders(): array
    {
        return Arr::only($this->headers, array_keys($this->getColumns()));
    }

    /**
     * Get the column names to show (lowercase table headers).
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return array_map('strtolower', $this->headers);
    }
}

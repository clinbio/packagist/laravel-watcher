<?php

namespace Dterumal\Watcher\Console;

use Dterumal\Watcher\Events\WatcherCreated;
use Dterumal\Watcher\Watcher;
use Dterumal\Watcher\WatcherOptions;
use Illuminate\Console\Command;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Contracts\Cache\Repository as Cache;

class WatchCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'watch:run
                            {--force : Force the worker to run even in maintenance mode}
                            {--once : Specify if the watcher should stop after one file has been detected}
                            {--sleep=3 : Number of seconds to sleep when no job is available}
                            {--rest=0 : Number of seconds to rest between jobs}
                            {--timeout=0 : Only process the event on the queue}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start monitoring the SFTP folder and its sub-directories for new files';

    /**
     * The cache store implementation.
     *
     * @var Repository
     */
    protected Repository $cache;

    /**
     * The inotify instance
     *
     * @var Watcher
     */
    protected Watcher $watcher;

    /**
     * Create a new queue restart command.
     *
     * @param  Watcher  $watcher
     * @param  Cache  $cache
     */
    public function __construct(Watcher $watcher, Cache $cache)
    {
        parent::__construct();

        $this->cache = $cache;
        $this->watcher = $watcher;
    }

    /**
     * Execute the console command.
     *
     * @return int|null
     */
    public function handle()
    {
        if ($this->downForMaintenance()) {
            $this->watcher->sleep($this->option('sleep'));
        }

        $this->listenForEvents();

        return $this->runWatcher();

    }

    /**
     * Run the worker instance.
     *
     * @return int|null
     */
    protected function runWatcher(): ?int
    {
        return $this->watcher
            ->setCache($this->cache)
            ->daemon(
                $this->gatherWorkerOptions()
            );
    }

    /**
     * Gather all of the queue worker options as a single object.
     *
     * @return WatcherOptions
     */
    protected function gatherWorkerOptions(): WatcherOptions
    {
        return new WatcherOptions(
            $this->option('sleep'),
            $this->option('force'),
            $this->option('rest'),
            $this->option('timeout'),
            $this->option('once')
        );
    }

    /**
     * Listen for the queue events in order to update the console output.
     *
     * @return void
     * @psalm-suppress UndefinedInterfaceMethod
     */
    protected function listenForEvents(): void
    {
        $this->laravel['events']->listen(WatcherCreated::class, function ($event) {
            $this->comment('- Watching: '.$event->directory);
        });
    }

    /**
     * Determine if the watcher should run in maintenance mode.
     *
     * @return bool
     */
    protected function downForMaintenance(): bool
    {
        return !$this->option('force') && $this->laravel->isDownForMaintenance();
    }
}

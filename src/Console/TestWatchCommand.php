<?php

namespace Dterumal\Watcher\Console;

use Illuminate\Console\Command;

class TestWatchCommand extends Command
{
    protected $hidden = true;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'watch:test';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        dispatch(function() {
            $this->callSilently('watch:run', [
                '--timeout' => 10
            ]);
        });

    }
}

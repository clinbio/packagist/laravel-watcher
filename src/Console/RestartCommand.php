<?php

namespace Dterumal\Watcher\Console;

use Dterumal\Watcher\Events\WatcherRestarted;
use Illuminate\Console\Command;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Support\InteractsWithTime;

class RestartCommand extends Command
{
    use InteractsWithTime;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'watch:restart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restart the watcher';

    /**
     * The cache store implementation.
     *
     * @var Repository
     */
    protected Cache $cache;

    /**
     * Create a new command instance.
     *
     * @param  Cache  $cache
     * @return void
     */
    public function __construct(Cache $cache)
    {
        parent::__construct();

        $this->cache = $cache;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->cache->forever('watcher:restart', $this->currentTime());

        $this->info('Broadcasting watcher restart signal.');
    }
}

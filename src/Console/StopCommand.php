<?php

namespace Dterumal\Watcher\Console;

use Dterumal\Watcher\Events\WatcherStopped;
use Dterumal\Watcher\Events\WatcherStopping;
use Illuminate\Console\Command;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Contracts\Cache\Repository as Cache;

class StopCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'watch:stop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Force the watcher to stop';

    /**
     * The cache store implementation.
     *
     * @var Repository
     */
    protected Cache $cache;

    /**
     * Create a new command instance.
     *
     * @param  Cache  $cache
     * @return void
     */
    public function __construct(Cache $cache)
    {
        parent::__construct();

        $this->cache = $cache;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->cache->forever('watcher:stop', true);

        $this->info('Broadcasting watcher stop signal.');
    }
}
